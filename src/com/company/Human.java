package com.company;

public abstract class Human {

    private String firstName;
    private String lastName;
    private String gender;
    private double age;
    private double height;
    private int children;
    private String maritalStatus;
    private String profession;

    public Human(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }
        public Human (String gender, double age,double height) {
            this.gender = gender;
            this.age = age;
            this.height = height;
        }
    public Human(String children, String maritalStatus, String profession){
        this.children = children;
        this.maritalStatus = maritalStatus;
        this.profession = profession;
    }

    // create void methods
    void speak(String words) {
        System.out.println(words);
    }

    void eat(String food) {
        System.out.println("I eat " + food);

    }

    void sleep() {
        System.out.println("I like to sleep");

    }

    void walk() {

    }

    void read(String books) {
        System.out.println("Reading " + books + "...");

    }

    void giveBirthTo(int numberOfChildren) {
        children += numberOfChildren;
    }

    String marriage(String family) {
        return family;
    }


    // create return methods
    String work(String salary) {
        return salary;
    }

    String cook(String dish) {
        return dish;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        System.out.println(gender);
    }

    public double getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        System.out.println("I am" + height);
    }

    public int getChildren() {
        return children;
    }

    public void setChildren(int children) {
        System.out.println("I have 2 children");
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;

    }
}