package com.company;

public class Girl extends Woman {

    public Girl(String gender, double age, double height) {
        super(gender, age, height);
    }

    @Override
    void speak(String words) {
        super.speak(words);
    }

    @Override
    void sleep() {
        super.sleep();
    }

    @Override
    void walk() {
        super.walk();
    }

    @Override
    void read(String books) {
        super.read(books);
    }
    void goToSchool() {
        System.out.println("I go to school every day");

    }


}
