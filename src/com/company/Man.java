package com.company;

public class Man extends Human {

    public String education;

    public Man(String firstName, String lastName) {
        super(firstName, lastName);
    }

    public Man(String gender, double age, double height) {
        super(gender, age, height);
    }

    public Man(String children, String maritalStatus,String profession, String education) {
        super(children, maritalStatus, profession);
        this.education = education;
    }



    @Override
    void speak(String words) {
        super.speak(words);
    }

    @Override
    void eat(String food) {
        super.eat(food);
    }

    @Override
    void sleep() {
        super.sleep();
    }

    @Override
    void walk() {
        super.walk();
    }

    @Override
    void read(String books) {
        super.read(books);
    }

    @Override
    void giveBirthTo(int numberOfChildren) {
        super.giveBirthTo(numberOfChildren);
    }

    @Override
    String marriage(String family) {
        return super.marriage(family);
    }

    @Override
    String work(String salary) {
        return super.work(salary);
    }

    @Override
    String cook(String dish) {
        return super.cook(dish);
    }
}
