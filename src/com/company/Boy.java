package com.company;

public class Boy extends Man {

    public Boy(String firstName, String lastName) {
        super(firstName, lastName);
    }

    public  Boy (String gender, double age, double height) {
        super(gender, age, height);
    }

    @Override
    void speak(String words) {
        super.speak(words);
    }

    @Override
    void eat(String food) {
        super.eat(food);
    }

    @Override
    void sleep() {
        super.sleep();
    }

    @Override
    void walk() {
        super.walk();
    }

    @Override
    void read(String books) {
        super.read(books);
    }

    @Override
    public String getFirstName() {
        return super.getFirstName();
    }

    @Override
    public void setFirstName(String firstName) {
        super.setFirstName(firstName);
    }

    @Override
    public String getLastName() {
        return super.getLastName();
    }

    @Override
    public void setLastName(String lastName) {
        super.setLastName(lastName);
    }

    @Override
    public String getGender() {
        return super.getGender();
    }

    @Override
    public void setGender(String gender) {
        super.setGender(gender);
    }

    @Override
    public double getAge() {
        return super.getAge();
    }

    @Override
    public void setAge(int age) {
        super.setAge(age);
    }

    @Override
    public double getHeight() {
        return super.getHeight();
    }

    @Override
    public void setHeight(double height) {
        super.setHeight(height);
    }
}
