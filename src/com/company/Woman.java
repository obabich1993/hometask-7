package com.company;

public class Woman extends Human {
    public String fullName;


    public Woman(int children, String maritalStatus, String profession, String fullName) {
        super(children, maritalStatus, profession);
        this.fullName = fullName;
    }
    public Woman(String gender, double age, double height) {
        super(gender, age, height);

    }

    @Override
    void sleep() {
        super.sleep();
    }

    @Override
    void walk() {
        super.walk();
    }

    @Override
    void read(String books) {
        super.read(books);
    }

    @Override
    String work(String salary) {
        return super.work(salary);
    }

    @Override
    String cook(String dish) {
        return super.cook(dish);
    }
}
