package com.company;

public class Main {

    public static void main(String[] args) {
        Human Jane = new Woman("female", 29, 1.80);
        Jane.getChildren();
        Human John = new Man("John", "Smith");
        John.getGender();
        Human Ann = new Girl("female", 12, 1.30);
        Ann.getAge();
        Human Dan = new Boy("Dan", "Smith");
        Dan.getLastName();


    }

}
